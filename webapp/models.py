from django.db import models
from urllib import quote_plus
from settings import google_api_key
from django.contrib.auth.models import User

class UserProfile(models.Model):
    user = models.OneToOneField(User)


class Property(models.Model):
    name = models.CharField(max_length=50)
    manager = models.ManyToManyField(User, related_name='manager')
    owner = models.ManyToManyField(User, related_name='owner')
    address = models.CharField(max_length=50)
    address2 = models.CharField(max_length=50, null=True, verbose_name='second address line')
    zip_code = models.CharField(max_length=50)
    description = models.TextField(max_length=1000, null=True)

    def __str__(self):
        return self.name + " (" + self.address + ")"

    class Meta:
        verbose_name_plural = "properties"


class Unit(models.Model):
    name = models.CharField(max_length=50)
    rent = models.DecimalField(max_digits=6, decimal_places=2)
    property = models.ForeignKey(Property)
    tenant = models.ManyToManyField(User)
    address = models.CharField(max_length=50)
    address2 = models.CharField(max_length=50, null=True)
    zip_code = models.CharField(max_length=50)
    description = models.TextField(max_length=1000, null=True)

    def get_query_location(self):
        return quote_plus(self.address + " " + self.zip_code)

    def get_street_view(self):
        url = 'http://maps.googleapis.com/maps/api/streetview?size=200x100&location={0}&fov=120&sensor=false'
        return url.format(self.get_query_location())

    def get_map(self):
        url = 'https://www.google.com/maps/embed/v1/place?key={0}&q={1}'
        print url.format(google_api_key, self.get_query_location())

    def __str__(self):
        return self.name + " (" + self.address + ")"


class Room(models.Model):
    name = models.CharField(max_length=50)
    unit = models.ForeignKey(Unit)
    description = models.TextField(max_length=1000, null=True)
    width = models.DecimalField(max_digits=5, decimal_places=3, null=True)
    height = models.DecimalField(max_digits=5, decimal_places=3, null=True)


class Appliance(models.Model):
    name = models.CharField(max_length=50)
    room = models.ForeignKey(Room)
    cost = models.DecimalField(max_digits=2, decimal_places=2, null=True)
    xcoord = models.IntegerField(max_length=5, null=True)
    ycoord = models.IntegerField(max_length=5, null=True)
    width = models.IntegerField(max_length=5, null=True)
    height = models.IntegerField(max_length=5, null=True)
    weight = models.DecimalField(max_digits=5, decimal_places=2, null=True)
    metric = models.BooleanField(default=False)


class Sensor(models.Model):
    name = models.CharField(max_length=50)
    room = models.ForeignKey(Room)
    active = models.BooleanField(default=False)
    xcoord = models.IntegerField(max_length=5, null=True)
    ycoord = models.IntegerField(max_length=5, null=True)


class Light(Appliance):
    on = models.BooleanField(default=False)
    bulb = models.CharField(max_length=50, null=True)


class Thermostat(Appliance):
    temperature = models.DecimalField(max_digits=3, decimal_places=2, default=70.0)

