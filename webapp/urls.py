__author__ = 'zalfonse'
from django.conf.urls import patterns, url
from views import IndexView, LoginView, PropertyView

urlpatterns = patterns('',
    url(r'^$', IndexView.as_view(), name='index'),
    url(r'^property/(?P<id>\d+)$', PropertyView.as_view(), name='property'),
    url(r'^login/.*$', LoginView.as_view(), name='login'),
    url(r'^logout/.*$', 'webapp.views.logout', name='logout')
)