from django.views.generic import ListView, FormView
from django.core.urlresolvers import reverse_lazy
from django.contrib.auth import forms
from django.contrib.auth.views import logout_then_login
from django.contrib import auth


from models import Property, Unit
from mixins import LoginRequiredMixin


class IndexView(LoginRequiredMixin, ListView):
    template_name = 'webapp/index.html'
    context_object_name = 'properties'
    model = Property

    def get_queryset(self):
        return Property.objects.filter(owner=self.request.user)


class PropertyView(LoginRequiredMixin, ListView):
    template_name = 'webapp/property.html'
    context_object_name = 'units'
    model = Unit

    def get_queryset(self):
        return Unit.objects.filter(property=self.kwargs['id'])


class StatusView(ListView):
    template_name = 'webapp/index.html'
    context_object_name = 'units'
    model = Unit


def logout(request):
    return logout_then_login(request)


class LoginView(FormView):
    template_name = 'webapp/login.html'
    success_url = reverse_lazy('webapp:index')
    form_class = forms.AuthenticationForm

    def form_valid(self, form):
        result = super(LoginView, self).form_valid(form)
        auth.login(self.request, form.get_user())
        return result